//
// Created by jeroen on 6/13/15.
//

#ifndef COLDFUSION_SCANNER_H
#define COLDFUSION_SCANNER_H

#include <iostream>
#include "piface.h"
#include "database.h"

class scanner
{
public:
    // Constructors and Destructors
    scanner();
    ~scanner();

    // Public routines
    void startScanning(); // Starts the scanning process

private:
    // Private variables
    pifaceController piControll;    // A controller for the piface class
    cfDatabase cfDb;                // A database controller
    std::string uid;                // Stores the users uid
};
#endif //COLDFUSION_SCANNER_H
