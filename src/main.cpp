#include "utils.h"
#include "scanner.h"

int main(int argc, char **argv)
{
    scanner scan;

    printBanner();  //Prints main banner.
    scan.startScanning();

    return 0;
}