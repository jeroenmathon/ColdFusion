#include "piface.h"

pifaceController::pifaceController()
{

}

pifaceController::~pifaceController()
{

}

void pifaceController::pinSet(int pin,int mode){
    std::stringstream command;

    // Parse the information into a command.
    command << "gpio -p write " << pin << " " << mode;

    // Create a constant char from the string stream.
    const char *Command = command.str().c_str();

    // Run the command
    system(Command);
}

void pifaceController::openDoor(int pin)
{
    pinSet(pin,1);
    sleep(5);
    pinSet(pin,0);
}