//
// Created by jeroen on 6/13/15.
//

#ifndef COLDFUSION_DATABASE_H
#define COLDFUSION_DATABASE_H
#define DB "/opt/coldfusion/cf.db"

#include <sqlite3.h>
#include <iostream>
#include <vector>

class cfDatabase
{
public:
    // Constructors and Destructors
    cfDatabase();
    ~cfDatabase();

    // Public routines
    bool isInDatabase(std::string uid);                 // Checks if user is in the database
    void addUser(std::string uid, std::string name);    // Adds a new user to the database
    void deleteUser(std::string id);                    // Removes a user from the database
    void modifyUser(std::string id, std::string name);  // Updates the name of a user in the database
    void printFull();                                   // Print out all the users and their data

private:
    // Private routines
    int connectDatabase();
    int disconnectDatabase();
    static int callback(void *NotUsed, int argc, char **argv, char **azColName);
};


#endif //COLDFUSION_DATABASE_H
