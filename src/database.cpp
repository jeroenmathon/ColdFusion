//
// Created by jeroen on 6/13/15.
//

#include "database.h"

struct ustruct  // User structure
{
    std::vector<int> id;            // The users ID <Used to lookup records in the database>
    std::vector<std::string> uid;   // The users UID
    std::vector<std::string> name;  // The users name
    unsigned long uCount;           // The amount of users loaded into the vector
    void clear();                   // A function to clear this structure
};
char* errMsg;   // A variable to hold error messages in
sqlite3* db;    // A pointer to the sqlite3 database
ustruct users;  // The structure to hold users

void ustruct::clear()
{
    id.clear();
    uid.clear();
    name.clear();
    uCount = 0;
}

cfDatabase::cfDatabase()
{

}

cfDatabase::~cfDatabase()
{

}

int cfDatabase::callback(void *NotUsed, int argc, char **argv, char **azColName)
{
    sqlite3_stmt *stmt;
    /* Get uid */
    char *query = "SELECT uid FROM users";

    if(sqlite3_prepare(db, query, -1, &stmt, 0) == SQLITE_OK)
    {
        int ctotal = sqlite3_column_count(stmt);

        int res = 0;

        while(1)
        {
            res = sqlite3_step(stmt);

            if(res == SQLITE_ROW)
            {
                for(int i(0); i<ctotal; i++)
                {
                    std::string s = (char*)sqlite3_column_text(stmt, i);
                    users.uid.push_back(s);
                }
            }

            if(res == SQLITE_DONE || res == SQLITE_ERROR) break;
        }
    }
    /* Get name */
    char* query2 = "SELECT name FROM users";

    if(sqlite3_prepare(db, query2, -1, &stmt, 0) == SQLITE_OK)
    {
        int ctotal = sqlite3_column_count(stmt);

        int res = 0;

        while(1)
        {
            res = sqlite3_step(stmt);

            if(res == SQLITE_ROW)
            {
                for(int i(0); i<ctotal; i++)
                {
                    users.name.push_back((char*)sqlite3_column_text(stmt, i));
                }
            }

            if(res == SQLITE_DONE || res == SQLITE_ERROR) break;
        }
    }
    /* Get id */
    char *query3 = "SELECT ID FROM users";

    if(sqlite3_prepare(db, query3, -1, &stmt, 0) == SQLITE_OK)
    {
        int ctotal = sqlite3_column_count(stmt);

        int res = 0;

        while(1)
        {
            res = sqlite3_step(stmt);

            if(res == SQLITE_ROW)
            {
                for(int i(0); i<ctotal; i++)
                {
                    int s = sqlite3_column_int(stmt, i);
                    users.id.push_back(s);
                }
            }

            if(res == SQLITE_DONE || res == SQLITE_ERROR) break;
        }
    }
    /* Set vector size. */
    users.uCount = users.uid.size();
    //return 0;
}

/* Opens a connection to the database. */
int cfDatabase::connectDatabase()
{

    if(sqlite3_open(DB, &db) == 1)
    {
        std::cout << "[ERROR]:" << sqlite3_errmsg(db) << "\n";
        return -1;
    }
    else
    {   //Create table if it does not exist.
        if(sqlite3_exec(db,
                        "CREATE TABLE IF NOT EXISTS users(ID INTEGER PRIMARY KEY AUTOINCREMENT, uid TEXT, name TEXT);",
                        callback, 0, &errMsg) == 1) std::cout << "[ERROR]:" << sqlite3_errmsg(db) << "\n";
    }
    return 0;
}
/* Closes the connection to the database. */
int cfDatabase::disconnectDatabase()
{
    sqlite3_close(db);
    users.clear();
    return 0;
}
/* Checks if user is in the database. */
bool cfDatabase::isInDatabase(std::string uid)
{
    connectDatabase();
    std::string input = "SELECT * FROM users;";
    if(sqlite3_exec(db, input.c_str(), callback, 0, &errMsg) == 1) std::cout << "[ERROR]:" << sqlite3_errmsg(db) << "\n";
    for(unsigned long int i(0); i != users.uCount; i++)
    {
        if(users.uid.at(i) == uid)
        {
            disconnectDatabase();
            return true;
        }
    }

    disconnectDatabase();
    return false;
}

void cfDatabase::printFull()
{
    connectDatabase();
    std::string input = "SELECT * FROM users;";

    if(sqlite3_exec(db, input.c_str(), callback, 0, &errMsg) == 1) std::cout << "[ERROR]:" << sqlite3_errmsg(db) << "\n";

    std::cout << "USER LIST START:\n\n";
    for(unsigned long int i(0); i != users.uCount; i++)
    {
        std::cout << "ID:" << users.id.at(i) << " |UID:" << users.uid.at(i) << " |NAME:" << users.name.at(i) << "\n";
    }
    std::cout << "\n\nUSER LIST END:\n\n";
    disconnectDatabase();
}

void cfDatabase::addUser(std::string uid, std::string name)
{
    connectDatabase();

    std::string input = "INSERT INTO users(uid, name) VALUES(\""+uid+"\", \""+name+"\");";
    if(sqlite3_exec(db, input.c_str(), callback, 0, &errMsg) == 1) std::cout << "[ERROR]:" << sqlite3_errmsg(db) << "\n";

    disconnectDatabase();
}

void cfDatabase::deleteUser(std::string id)
{
    printFull();
    connectDatabase();

    std::string input = "DELETE FROM users WHERE ID="+id+";";
    if(sqlite3_exec(db, input.c_str(), callback, 0, &errMsg) == 1) std::cout << "[ERROR]:" << sqlite3_errmsg(db) << "\n";

    disconnectDatabase();

}

void cfDatabase::modifyUser(std::string id, std::string name)
{
    printFull();
    connectDatabase();

    std::string input = "UPDATE users SET name=\""+name+"\" WHERE ID="+id+";";
    if(sqlite3_exec(db, input.c_str(), callback, 0, &errMsg) == 1) std::cout << "[ERROR]:" << sqlite3_errmsg(db) << "\n";

    disconnectDatabase();
}