//
// Created by jeroen on 6/13/15.
//

#ifndef COLDFUSION_PIFACE_H
#define COLDFUSION_PIFACE_H

#include <iostream>
#include <sstream>
#include <string>
#include <stdlib.h>
#include <unistd.h>

class pifaceController
{
public:
    // Constructors and Destructors
    pifaceController();
    ~pifaceController();

    // Public routines
    void openDoor(int pin); // Opens the door using pin set

private:
    // Private routines
    void pinSet(int pin, int mode); // Sets the state of a pin in the piface board
};

#endif //COLDFUSION_PIFACE_H
