//
// Created by jeroen on 6/14/15.
//

#include <iostream>
#include "database.h"

int main(int argc, char **argv)
{
    cfDatabase cfDb; // Database controller
    system("clear");
    std::string option;
    bool exitFlag = false;

    while(!exitFlag)
    {
        std::cout << "\n1.Add new users.\n2.Modify users.\n3.Remove users.\n4.List users\n5.Exit.\nOption:";
        std::cin >> option;

        if(option == "1")   //Add new users
        {
            cfDb.printFull();

            std::string uid;
            std::string name;

            std::cout << "UID:";
            std::cin >> uid;
            std::cout << "NAME[FIRST_LAST]:";
            std::cin >> name;

            cfDb.addUser(uid, name);
        }
        else if(option == "2")  //Modify users
        {
            cfDb.printFull();
            std::string id;
            std::string name;
            std::cout << "ID:";
            std::cin >> id;
            std::cout << "NEW NAME[FIRST_LAST]:";
            std::cin >> name;

            cfDb.modifyUser(id, name);
        }
        else if(option == "3")  //Remove users
        {
            cfDb.printFull();

            std::string id;

            std::cout << "ID:";
            std::cin >> id;

            cfDb.deleteUser(id);
        }
        else if(option == "4")  //List users
        {
            cfDb.printFull();
            std::string op;
            while(true)
            {
                std::cout << "Type \"q\" to return:";
                std::cin >> op;
                if(op != "q" || op != "Q" || op != "\"q\"" || op != "\"Q\"") break;
            }

        }
        else if(option == "5")  //Exit
        {
            exitFlag = true;
        }
        else
        {
            std::cout << "INVALID OPTION!\n\n";
        }

        system("clear");
    }
    return 0;
}