//
// Created by jeroen on 6/13/15.
//

#include "utils.h"

void printBanner()
{
    std::cout
    << " ________________________\n"
    << "|   #####   |	######## |\n"
    << "| ##        |   #        |\n"
    << "|#          |   ######## |\n"
    << "|#          |   #        |\n"
    << "| ##        |   #        |\n"
    << "|  ######   |   #        |\n"
    << "|___Cold____|___Fusion___|\n"
    << "|_Made_by__Jeroen_Mathon_|\n";
}