ColdFusion
==========

ColdFusion is a Raspberry pi RFID access control system.
It uses [wiringPi](http://wiringpi.com/) to control the Piface that toggle's the electronic lock in the door.

It then compares the UID given from the Scanner to the ones in the sqlite database,
If a match is found it will open the doors lock.

## IMPORTANT!!!
Be sure to create a directory in the /opt path that's called coldfusion that has the (777 permission or is chowned by the user that runs the program).

## Setup instructions.
First edit the /etc/inittab file by typing ```sudo nano /etc/inittab ```.

Then look for a line similar to this ```1:2345:respawn:/sbin/getty --noclear 38400 tty1```
And change it to ```1:2345:respawn:/sbin/getty --autologin --noclear 38400 tty1```

After that make a new account called main by typing ```sudo adduser -m -g users -s /bin/bash main```
And then setting a password for it using ```sudo passwd main```

The next step is to create a new directory in /opt.
Type the following as the user pi to create a directory for coldfusion ```sudo mkdir /opt/coldfusion``
Then make it so that everyone can put files in it by typing ```sudo chmod 777 /opt/coldfusion```

The next step is to build and install the software.
First navigate to the directory where you cloned this git repository to.

Then create a new directory in there called build and enter it.
Now we need some software before we can build this program.
Install the following packages using this command on raspbian. ```sudo apt-get install libsqlite3-dev g++-4.7 cmake```

Then update the current c++ compiler by linking the new binary to its shortcut.
Do this by first removing the symlink and then making a new one leading to the new g++ version.
Type the following command to do this ```sudo rm /usr/bin/c++ ; sudo ln -s /usr/bin/g++-4.7 /usr/bin/c++```

Next up if your still in the build directory type ```cmake ..``` to generate a MakeFile next up type ```make``` and ```sudo make install``` to compile and install the program.

Now we are going to run coldfusion once by typing ```coldfusion``` this should generate a database file.
Now close the program by pressing the control key and c (ctrl+c).
Next enter the /opt/coldfusion directory by typing ```cd /opt/coldfusion/``` now set the chmod of the file cf.db to 777 by typing ```chmod 777 cf.db```.

Now it is time to setup the users to automatically start the program on boot.
Make sure that the raspberry boots into console mode.
You can check this by typing ```sudo raspi-config``` and then heading to "2. Enable Boot to Desktop/Scratch.".
You want to set it to console mode.

When it promts you to reboot press no.

Next as the user pi open up your .profile file.
you can do this by typing the following command ```cd ; nano .profile```
Add the following to the end of this file. ```coldfusion```

Next up switch to the main user by typing ```sudo su main```
Then do the same by typing ```cd ; nano .profile```
But now instead of ```coldfusion``` you add ```coldfusion-usercon``` to the end of the file.```

Now switch back to the pi user by typing ```exit```

Now make sure that you have setup ssh to this box.
You can do that by installing openssh,
run ```sudo apt-get install openssh```
Then run ```sudo service sshd enable```
To start it its either that or ```sudo service openssh enable`` it is different for some distros.

Now before we reboot type ```ifconfig``` and write down its internal ip so you can remote connect to it by using putty or  ssh.

When it finished booting it should greet you with the coldfusion prompt.

Now connect your Electronic lock and RFID USB reader and enjoy.

You can change the pin numbers in the piface.cpp file.
